package com.example.assignment_4;

import android.content.Intent;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

public class WeatherActivity extends AppCompatActivity {

    private String cityName;
    String cityN, country, windDir, weatherDesc, weatherIconUrl;
    Integer temp, windSpeed, preCip, humidity;
    private Gson gson;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_weather);

        Intent intent = getIntent();
        cityName = intent.getExtras().getString("cityName");

        weather();
    }
    public void weather(){
        String url = "http://api.weatherstack.com/current" +
                "?access_key=5cde0aaaa12917785c5036825e2a1266" +
                "&query=" + cityName ;
        JSONObject jsonObject = new JSONObject();

        ImageView weatherIcon = findViewById(R.id.weatherIcon);
        TextView degree = findViewById(R.id.degree);
        TextView city = findViewById(R.id.city);
        TextView desc = findViewById(R.id.desc);
        TextView wdir = findViewById(R.id.wdir);
        TextView wspeed = findViewById(R.id.wspeed);
        TextView pcip = findViewById(R.id.pcip);
        TextView hum = findViewById(R.id.hum);


        Response.Listener<JSONObject> successBlock = new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {
               try{
                   //Using Gson
                    Gson gson = new Gson();
                    WeatherData weatherData;
                    weatherData = gson.fromJson(response.toString(), WeatherData.class);
                    cityN = weatherData.getLocation().getName();
                    country = weatherData.getLocation().getCountry();

                    temp = weatherData.getCurrent().getTemperature();
                    windDir = weatherData.getCurrent().getWind_dir();
                    windSpeed = weatherData.getCurrent().getWind_speed();
                    preCip = weatherData.getCurrent().getPrecip();
                    humidity = weatherData.getCurrent().getHumidity();

                    city.setText(cityN.toString() +", "+country);
                    degree.setText(temp.toString()+"°");
                    wdir.setText("Wind Direction: " + windDir);
                    wspeed.setText("Wind Speed: " + windSpeed.toString() +" km/h");
                    pcip.setText("Precip: " + preCip.toString());
                    hum.setText("Humidity: " + humidity.toString());

                    //For JsonArray
                   JSONObject current = response.getJSONObject("current");
                   JSONArray weather_description = current.getJSONArray("weather_descriptions");
                   weatherDesc = weather_description.getString(0);
                   JSONArray weather_icons = current.getJSONArray("weather_icons");
                   weatherIconUrl = weather_icons.getString(0);
                   Glide.with(WeatherActivity.this).load(weatherIconUrl).into(weatherIcon);
                   desc.setText(weatherDesc);

                    //Toast.makeText(WeatherActivity.this , cityN + country, Toast.LENGTH_SHORT).show();


/*
                    JSONObject location =response.getJSONObject("location");
                    cityN = location.getString("name");
                    country = location.getString("country");

                    JSONObject current = response.getJSONObject("current");
                    temp = current.getInt("temperature");
                    windSpeed = current.getInt("wind_speed");
                    windDir = current.getString("wind_dir");
                    preCip = current.getInt("precip");
                    humidity = current.getInt("humidity");

                    JSONArray weather_description = current.getJSONArray("weather_descriptions");
                    weatherDesc = weather_description.getString(0);

                    JSONArray weather_icons = current.getJSONArray("weather_icons");
                    weatherIconUrl = weather_icons.getString(0);

                    Glide.with(WeatherActivity.this).load(weatherIconUrl).into(weatherIcon);
                    degree.setText(temp.toString()+"°");
                    city.setText(cityN.toString() +", "+country);
                    desc.setText(weatherDesc);
                    wdir.setText("Wind Direction: " + windDir);
                    wspeed.setText("Wind Speed: " + windSpeed.toString() +" km/h");
                    pcip.setText("Precip: " + preCip.toString());
                    hum.setText("Humidity: " + humidity.toString());
*/


                }
                catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(WeatherActivity.this, e.getMessage() , Toast.LENGTH_LONG).show();
                }

            }
        };

        Response.ErrorListener failureBlock = new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(WeatherActivity.this, "Error. " + error.getMessage(), Toast.LENGTH_LONG).show();
            }
        };

        JsonObjectRequest request =new JsonObjectRequest(Request.Method.GET, url, jsonObject, successBlock, failureBlock);
        Volley.newRequestQueue(this).add(request);

    }
    public void onBackPressed() {
        finish();
    }

}
