package com.example.week4.ui.students;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.week4.R;
import com.example.week4.databinding.FragmentStudentsBinding;
import com.example.week4.ui.courses.CoursesAdapter;

import java.util.ArrayList;


public class StudentsFragment extends Fragment {

    private FragmentStudentsBinding binding;

    // Add RecyclerView member
    private RecyclerView recyclerView;

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_students, container, false);

        ArrayList<String> itemsData = new ArrayList<>();
        itemsData.add("Salman");
        itemsData.add("Ahmed");
        itemsData.add("Shoukat");
        itemsData.add("Ali");

        // Add the following lines to create RecyclerView
        recyclerView = view.findViewById(R.id.recyclerview);
        recyclerView.setLayoutManager(new LinearLayoutManager(view.getContext()));
        recyclerView.setAdapter(new CoursesAdapter(itemsData));

        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }
}