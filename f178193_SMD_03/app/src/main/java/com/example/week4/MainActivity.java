package com.example.week4;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        String semail = getIntent().getStringExtra("email");
        String password = getIntent().getStringExtra("pass");

        String fullname = getIntent().getStringExtra("fullname");

        EditText email = findViewById(R.id.email);
        EditText pass = findViewById(R.id.password);

        Button loginbtn = findViewById(R.id.login);
        Button signupbtn = findViewById(R.id.signup);

        loginbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                if (email.getText().toString().equals(semail) && pass.getText().toString().equals(password)) {
//                    Intent user_main = new Intent(MainActivity.this, UserMain.class);
//                    user_main.putExtra("fullname",fullname.toString());
//                    Toast.makeText(getApplicationContext(), "Logged In Successfully.", Toast.LENGTH_SHORT).show();
//                    startActivity(user_main);
//                    finish();
//                }
//                else{
//                    AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
//                    builder.setMessage("Your Email does not match to student's Email. Please Try Again.");
//                    builder.setTitle("Failed!");
//                    AlertDialog alertDialog = builder.create();
//                    alertDialog.show();
//                }
                Intent menu = new Intent(MainActivity.this, Main_Menu.class);
                startActivity(menu);
            }
        });

        signupbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent signup_activity = new Intent(MainActivity.this, Signup.class);
                startActivity(signup_activity);
                finish();
            }
        });

    }
}