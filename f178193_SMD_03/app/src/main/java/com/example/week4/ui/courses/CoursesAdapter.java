package com.example.week4.ui.courses;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.week4.R;

import java.util.List;
import java.util.Random;

public class CoursesAdapter extends RecyclerView.Adapter<RecyclerViewHolder> {
    private List<String> mData;

    public CoursesAdapter(List<String> data ) {
        this.mData = data;
    }

    @Override
    public int getItemViewType(final int position) {
        return R.layout.frame_textview;
    }

    @NonNull
    @Override
    public RecyclerViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(viewType, parent, false);
        return new RecyclerViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerViewHolder holder, int position) {
        String itemsData = mData.get(position);
        holder.getView().setText(itemsData);
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }
}