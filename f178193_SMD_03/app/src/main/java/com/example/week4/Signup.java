package com.example.week4;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import android.telephony.PhoneNumberFormattingTextWatcher;

import androidx.appcompat.app.AppCompatActivity;

import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class Signup extends AppCompatActivity {

    public static boolean isValidEmail(CharSequence target) {
        return (!TextUtils.isEmpty(target) && Patterns.EMAIL_ADDRESS.matcher(target).matches());
    }
    private boolean isEmpty(EditText etText) {
        if (etText.getText().toString().trim().length() > 0) {
            return false;
        }
        etText.setError(etText.getHint().toString() + " is empty.");
        return true;
    }
    public boolean isValidPassword(final String password) {
        Pattern pattern;
        Matcher matcher;

        final String PASSWORD_PATTERN = "^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&+=])(?=\\S+$).{4,}$";

        pattern = Pattern.compile(PASSWORD_PATTERN);
        matcher = pattern.matcher(password);

        return matcher.matches();
    }

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.signup);

        EditText fname = findViewById(R.id.fname);
        EditText lname = findViewById(R.id.lname);
        EditText email = findViewById(R.id.email);
        EditText pass = findViewById(R.id.password);
        EditText re_pass = findViewById(R.id.re_password);
        EditText phone = findViewById(R.id.phone_no);
        EditText address = findViewById(R.id.address);

        phone.addTextChangedListener(new PhoneNumberFormattingTextWatcher());

        Button loginbtn = findViewById(R.id.login);
        Button signupbtn = findViewById(R.id.signup);
        signupbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                boolean valid = false;

                isEmpty(fname);
                isEmpty(lname);
                isEmpty(pass);
                isEmpty(re_pass);
                isEmpty(phone);
                isEmpty(email);

                if (!isEmpty(fname)){
                    if(!isEmpty(lname)){
                        if(!isEmpty(pass)){
                            if(!isEmpty(re_pass)){
                                if(!isEmpty(phone)){
                                    if (!isValidEmail(email.getText().toString())) {
                                        Toast.makeText(getApplicationContext(), "Your Email is not correct.", Toast.LENGTH_SHORT).show();
                                    } else {
                                        if (pass.getText().toString().length()<8 ){
                                            Toast.makeText(getApplicationContext(), "Password too short.", Toast.LENGTH_SHORT).show();
                                        } else {
                                            if(!pass.getText().toString().equals(re_pass.getText().toString())){
                                                Toast.makeText(getApplicationContext(), "Password did not match.", Toast.LENGTH_SHORT).show();
                                            }
                                            else{
                                                if (isValidPassword(pass.getText().toString())){
                                                    valid=true;
                                                }
                                                else{
                                                    Toast.makeText(getApplicationContext(), "Password should at least contain 1 Uppercase, 1 LowerCase and a Special Character", Toast.LENGTH_SHORT).show();
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

                if (valid){
                    Intent login_Activity = new Intent(Signup.this, MainActivity.class);
                    String fullname = fname.getText().toString() + " " + lname.getText().toString();
                    login_Activity.putExtra("email",email.getText().toString());
                    login_Activity.putExtra("pass", pass.getText().toString());
                    login_Activity.putExtra("fullname", fullname.toString());
                    Toast.makeText(getApplicationContext(), "Registration Successful.", Toast.LENGTH_SHORT).show();
                    startActivity(login_Activity);
                    finish();
                }

            }
        });

        loginbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent login_activity = new Intent(Signup.this, MainActivity.class);
                startActivity(login_activity);
                finish();
            }
        });

    }
}
